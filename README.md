SOAP Fibonacci Service

In order to build and run this project, please:

1. Download the project Fibonacci from https://gitlab.com/tashuer/FibonacciService
2. On docker enter to the folder Fibonacci
2. Execute ./start.sh
2. Go to http://localhost:8080/Fibonacci/FibonacciService?wsdl in your browser to see resulting the WSDL
3. Open the soapUI attached project
4. Run your tests!!! 

-----

The WSDL and XSD used in this project were generated with the "contract first" strategy in mind. If you want to generate your own artifacts from Java you can:

1. Go to the Project Forder
2. Run the following command: wsimport src/main/resources/fibonacci/fibonacci.wsdl -keep -Xnocompile -extension -p co.edu.javeriana.fibonacci.artifacts -d src/main/java/

That's gonna generate all the requeried artifacts that are referenced in the co.edu.javeriana.fibonacci.artifacts package
