FROM tashuer/payara
MAINTAINER Oscar Huertas - oscarhuertas@javeriana.edu.co
ENV WORKSPACE /opt/payara41/glassfish/domains/domain1/autodeploy
WORKDIR ${WORKSPACE}
ENV WAR_FILE Fibonacci.war
COPY target/${WAR_FILE} .
