
package co.edu.javeriana.fibonacci.artifacts;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the co.edu.javeriana.fibonacci.artifacts package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _FibonacciResponse_QNAME = new QName("http://www.servicios.co/fibonacci/v1", "fibonacciResponse");
    private final static QName _FibonacciRequest_QNAME = new QName("http://www.servicios.co/fibonacci/v1", "fibonacciRequest");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: co.edu.javeriana.fibonacci.artifacts
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link FibonacciResponseType }
     * 
     */
    public FibonacciResponseType createFibonacciResponseType() {
        return new FibonacciResponseType();
    }

    /**
     * Create an instance of {@link FibonacciRequestType }
     * 
     */
    public FibonacciRequestType createFibonacciRequestType() {
        return new FibonacciRequestType();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FibonacciResponseType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.servicios.co/fibonacci/v1", name = "fibonacciResponse")
    public JAXBElement<FibonacciResponseType> createFibonacciResponse(FibonacciResponseType value) {
        return new JAXBElement<FibonacciResponseType>(_FibonacciResponse_QNAME, FibonacciResponseType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FibonacciRequestType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.servicios.co/fibonacci/v1", name = "fibonacciRequest")
    public JAXBElement<FibonacciRequestType> createFibonacciRequest(FibonacciRequestType value) {
        return new JAXBElement<FibonacciRequestType>(_FibonacciRequest_QNAME, FibonacciRequestType.class, null, value);
    }

}
