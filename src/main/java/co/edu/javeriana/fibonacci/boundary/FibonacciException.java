package co.edu.javeriana.fibonacci.boundary;

public class FibonacciException extends Exception {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public FibonacciException() {
        super("N�mero no valido.  Debe ser un n�mero mayor a 0.");
    }
}
