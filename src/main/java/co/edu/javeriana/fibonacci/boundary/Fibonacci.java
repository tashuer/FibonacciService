package co.edu.javeriana.fibonacci.boundary;

import javax.jws.WebService;

import co.edu.javeriana.fibonacci.artifacts.FibonacciPortType;
import co.edu.javeriana.fibonacci.artifacts.FibonacciRequestType;
import co.edu.javeriana.fibonacci.artifacts.FibonacciResponseType;

@WebService(endpointInterface = "co.edu.javeriana.fibonacci.artifacts.FibonacciPortType")
public class Fibonacci implements FibonacciPortType {

	public FibonacciResponseType fibonacci(FibonacciRequestType fibonacciRequest) throws FibonacciException {
		if (fibonacciRequest.getNumber() <= 0) {
			throw new FibonacciException();
		}
		FibonacciResponseType fibonacci = new FibonacciResponseType();
		String sequence = "1,";
		for (long i = 2; i <= fibonacciRequest.getNumber(); i++) {
			sequence = sequence + fibonacci(i) + ",";
		}
		fibonacci.setSequence(sequence.substring(0, sequence.length()-1));
		return fibonacci;
	}
	
	private long fibonacci(long number) {
		if (number <= 1) { 
			return number;
		} else {
			return fibonacci(number-1) + fibonacci(number-2);
		}
	}

}
